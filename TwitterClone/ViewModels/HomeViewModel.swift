//
//  HomeViewModel.swift
//  TwitterClone
//
//  Created by Minh Hoàng Nguyễn Viết  on 14/05/2023.
//

import Foundation
import Combine
import FirebaseAuth

final class HomeViewModel: ObservableObject{
    @Published var user: TwitterUser?
    @Published var error: String?
    @Published var tweets: [Tweet] = []

    func retrieveUser() async {
        guard let id = Auth.auth().currentUser?.uid else {return }
        do{
            user = try await DatabaseManager.shared.collectionUsers(retrieve: id)
            await fetchTweets()
        }catch let failure {
            error = failure.localizedDescription
        }

    }

    func fetchTweets() async {
        guard let userID = user?.id else {return }
        do{
            tweets = try await DatabaseManager.shared.collectionTweets(retrieveTweets: userID)
        } catch let failure {
            error = failure.localizedDescription
        }
    }
}
