//
//  ProfileFormViewModel.swift
//  TwitterClone
//
//  Created by Minh Hoàng Nguyễn Viết  on 14/05/2023.
//

import Foundation
import Combine
import UIKit
import FirebaseStorage
import FirebaseAuth

final class ProfileFormViewModel: ObservableObject {
    @Published var displayName: String?
    @Published var username: String?
    @Published var bio: String?
    @Published var avatarPath: String?
    @Published var imageData: UIImage?
    @Published var isFormValid: Bool = false
    @Published var error: String?
    @Published var isOnboardingFinished: Bool = false

    func validateProfileForm() {
        guard let displayName = displayName, displayName.count > 2, let username = username, username.count > 2,
              let bio = bio, bio.count > 2, imageData != nil else {
            isFormValid = false
            return
        }

        isFormValid = true
    }

    func uploadAvatar() async {
        let randomId = UUID().uuidString
        guard let imageData = imageData?.jpegData(compressionQuality: 0.5) else { return }
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpeg"

        do{
            let metaData =  try await StorageManager.shared.uploadProfilePhoto(with: randomId, image: imageData, metaData: metaData)
            avatarPath = try await StorageManager.shared.downloadUrl(for: metaData.path).absoluteString
            await updateUserData()
        }catch let failure {
            error = failure.localizedDescription
        }
    }

    private func updateUserData() async{
        guard let displayName = displayName,
              let username = username,
              let bio = bio,
              let avatarPath = avatarPath,
              let id = Auth.auth().currentUser?.uid
        else {return }

        let updateFields: [String : Any] = [
            "displayName": displayName,
            "userName": username,
            "bio": bio,
            "avatarPath": avatarPath,
            "isUserOnBoarded": true
        ]

        do{
            try await DatabaseManager.shared.collectionUsers(updateFields: updateFields, for: id)
            isOnboardingFinished = true
        }catch let failure {
            error = failure.localizedDescription
        }
    }
}
