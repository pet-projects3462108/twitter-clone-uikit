//
//  ProfileViewModel.swift
//  TwitterClone
//
//  Created by Minh Hoàng Nguyễn Viết  on 15/05/2023.
//

import Foundation
import Combine
import FirebaseAuth

final class ProfileViewModel: ObservableObject{
    @Published var user: TwitterUser?
    @Published var error: String?

    func retreiveUser() async {
        guard let id = Auth.auth().currentUser?.uid else {return }
        do{
            user = try await DatabaseManager.shared.collectionUsers(retrieve: id)
        }catch let failure {
            error = failure.localizedDescription
        }
    }

    func getFormatedDate(with date: Double) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM YYYY"

        return dateFormatter.string(from: NSDate(timeIntervalSince1970: TimeInterval(floatLiteral: date)) as Date)
    }
}
