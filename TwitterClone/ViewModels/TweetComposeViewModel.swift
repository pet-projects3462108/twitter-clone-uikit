//
//  TweetComposeViewModel.swift
//  TwitterClone
//
//  Created by Minh Hoàng Nguyễn Viết  on 17/05/2023.
//

import Foundation
import Combine
import FirebaseAuth

final class TweetComposeViewModel: ObservableObject{
    @Published var tweetContent = ""
    @Published var isValidTweet = false
    @Published var error = ""
    @Published var shouldDismissComposer = false
    private var user: TwitterUser?

    func getUserData() async {
        guard let userID = Auth.auth().currentUser?.uid else {return }

        do{
            let user = try await DatabaseManager.shared.collectionUsers(retrieve: userID)
            self.user = user
        }catch let failure {
            error = failure.localizedDescription
        }
    }

    func validateTweet() {
        isValidTweet = !tweetContent.isEmpty
    }

    func createTweet() async {
        guard let user = user else {return}
        let tweet = Tweet(author: user, authorID: user.id, content: tweetContent, likesCount: 0, likers: [], isReply: false, parentReferenceId: nil)
        do {
            try await DatabaseManager.shared.collectionTweets(add: tweet)
            shouldDismissComposer = true
        }catch let failure {
            error = failure.localizedDescription
        }

    }
}
