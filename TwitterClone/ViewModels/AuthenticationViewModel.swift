//
//  RegistationViewModel.swift
//  TwitterClone
//
//  Created by Minh Hoàng Nguyễn Viết  on 07/05/2023.
//

import Foundation
import FirebaseAuth


final class AuthenticationViewModel: ObservableObject {
    @Published var email: String?
    @Published var password: String?
    @Published var isValidAuthenticationForm: Bool = false
    @Published var user: User?
    @Published var error: String?
    
    func validateRegistrationForm(){
        guard let email = email, let password = password else{
            isValidAuthenticationForm = false
            return
        }

        isValidAuthenticationForm = isValidEmail(email) && isValidPassword(password)
    }

    private func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }

    private func isValidPassword(_ password: String) -> Bool{
        return password.count >= 8
    }

    func createUser() async {
        guard let email = email, let password = password else {
            return
        }
        do {
            let authResult = try await AuthManager.shared.registerUser(email: email , password: password )
            try await DatabaseManager.shared.collectionUsers(add: authResult.user)
            user = authResult.user
        } catch let failure{
            error = failure.localizedDescription
        }

    }

    func login() async {
        guard let email = email, let password = password else {
            return
        }
        do {
            let authResult = try await AuthManager.shared.loginUser(email: email , password: password )
            user = authResult.user
        } catch let failure{
            error = failure.localizedDescription
        }

    }
}
