//
//  TweetTableViewCell.swift
//  TwitterClone
//
//  Created by Minh Hoàng Nguyễn Viết  on 17/04/2023.
//

import UIKit

protocol TweetTableViewCellDelegate: AnyObject {
    func tweetTableViewCellDidTapReply()
    func tweetTableViewCellDidTapRetweet()
    func tweetTableViewCellDidTapLike()
    func tweetTableViewCellDidTapShare()
}

class TweetTableViewCell: UITableViewCell {
    static let identifier = "TweetTableViewCell"

    weak var delegate: TweetTableViewCellDelegate?

    private let actionSpacing: CGFloat = 60
    private let avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 25
        imageView.layer.masksToBounds = true

        imageView.backgroundColor = .systemRed

        return imageView
    }()

    private let displayNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false

        label.font = .systemFont(ofSize: 18, weight: .bold)

        return label
    }()

    private let usernameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false

        label.textColor = .secondaryLabel
        label.font = .systemFont(ofSize: 16, weight: .regular)

        return label
    }()

    private let tweetTextContentLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0

        return label
    }()

    private lazy var actionButtons: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [replyButton, retweetButton, likeButton, shareButton])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = .equalSpacing

        return stackView
    }()

    private let replyButton: UIButton = actionButton(withIcon: "bubble.left")

    private let retweetButton: UIButton = actionButton(withIcon: "arrow.2.squarepath")

    private let likeButton: UIButton = actionButton(withIcon: "heart")

    private let shareButton: UIButton = actionButton(withIcon: "square.and.arrow.up")

    private static func actionButton(withIcon iconName: String) -> UIButton {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(systemName: iconName), for: .normal)
        button.tintColor = .systemGray2

        return button
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.addSubview(avatarImageView)
        contentView.addSubview(displayNameLabel)
        contentView.addSubview(usernameLabel)
        contentView.addSubview(tweetTextContentLabel)
        contentView.addSubview(actionButtons)
        configureConstraints()
        configureButtons()
    }

    private func configureConstraints() {
        let avatarImageViewConstraints = [
            avatarImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            avatarImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 14),
            avatarImageView.heightAnchor.constraint(equalToConstant: 50),
            avatarImageView.widthAnchor.constraint(equalToConstant: 50),
        ]

        let displayNameLabelConstraints = [
            displayNameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
            displayNameLabel.leadingAnchor.constraint(equalTo: avatarImageView.trailingAnchor, constant: 20),
        ]

        let usernameLabelConstraints = [
            usernameLabel.centerYAnchor.constraint(equalTo: displayNameLabel.centerYAnchor),
            usernameLabel.leadingAnchor.constraint(equalTo: displayNameLabel.trailingAnchor, constant: 10),
            usernameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
        ]

        displayNameLabel.setContentHuggingPriority(.defaultHigh, for: .horizontal)
//        usernameLabel.setContentHuggingPriority(.defaultLow, for: .horizontal)
//        displayNameLabel.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        usernameLabel.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)

        let tweetTextContentLabelConstraints = [
            tweetTextContentLabel.leadingAnchor.constraint(equalTo: displayNameLabel.leadingAnchor),
            tweetTextContentLabel.topAnchor.constraint(equalTo: displayNameLabel.bottomAnchor, constant: 10),
            tweetTextContentLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
        ]

        let actionButtonsConstraints = [
            actionButtons.leadingAnchor.constraint(equalTo: tweetTextContentLabel.leadingAnchor),
            actionButtons.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            actionButtons.topAnchor.constraint(equalTo: tweetTextContentLabel.bottomAnchor, constant: 10),
            actionButtons.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -15),
        ]

        NSLayoutConstraint.activate(avatarImageViewConstraints)
        NSLayoutConstraint.activate(displayNameLabelConstraints)
        NSLayoutConstraint.activate(usernameLabelConstraints)
        NSLayoutConstraint.activate(tweetTextContentLabelConstraints)
        NSLayoutConstraint.activate(actionButtonsConstraints)

    }

    @objc private func didTapReply() {
        delegate?.tweetTableViewCellDidTapReply()
    }

    @objc private func didTapLike() {
        delegate?.tweetTableViewCellDidTapLike()
    }

    @objc private func didTapShare() {
        delegate?.tweetTableViewCellDidTapShare()
    }

    @objc private func didTapRetweet() {
        delegate?.tweetTableViewCellDidTapRetweet()
    }

    private func configureButtons() {
        likeButton.addTarget(self, action: #selector(didTapLike), for: .touchUpInside)
        replyButton.addTarget(self, action: #selector(didTapReply), for: .touchUpInside)
        retweetButton.addTarget(self, action: #selector(didTapRetweet), for: .touchUpInside)
        shareButton.addTarget(self, action: #selector(didTapShare), for: .touchUpInside)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configureTweet(with model: Tweet) {
        displayNameLabel.text = model.author.displayName
        usernameLabel.text = "@\(model.author.userName)"
        tweetTextContentLabel.text = model.content
        avatarImageView.sd_setImage(with: URL(string: model.author.avatarPath))
    }
}
