//
//  TwitterUser.swift
//  TwitterClone
//
//  Created by Minh Hoàng Nguyễn Viết  on 14/05/2023.
//

import Foundation
import FirebaseAuth

struct TwitterUser: Codable{
    let id: String
    var displayName: String = ""
    var userName: String = ""
    var followerCount: Int = 0
    var followingCount: Int = 0
    var createdOn: Double
    var bio: String = ""
    var avatarPath: String = ""
    var isUserOnBoarded: Bool = false

    init(from user: User){
        id = user.uid
        createdOn = NSDate().timeIntervalSince1970.magnitude
    }
}
