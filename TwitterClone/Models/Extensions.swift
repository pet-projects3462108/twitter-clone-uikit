//
//  Extensions.swift
//  TwitterClone
//
//  Created by Minh Hoàng Nguyễn Viết  on 14/05/2023.
//

import Foundation

extension Encodable {
  var dictionary: [String: Any]? {
    guard let data = try? JSONEncoder().encode(self) else { return nil }
    return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
  }
}
