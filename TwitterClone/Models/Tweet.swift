//
//  Tweet.swift
//  TwitterClone
//
//  Created by Minh Hoàng Nguyễn Viết  on 17/05/2023.
//

import Foundation

struct Tweet: Codable, Identifiable{
    var id = UUID().uuidString
    let author: TwitterUser
    let authorID: String
    let content: String
    var likesCount: Int
    var likers: [String]
    let isReply: Bool
    let parentReferenceId: String?
}
