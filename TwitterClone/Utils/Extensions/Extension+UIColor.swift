//
//  Extension+UIColor.swift
//  TwitterClone
//
//  Created by Minh Hoàng Nguyễn Viết  on 17/05/2023.
//

import Foundation
import UIKit

extension UIColor{
    static let twitterBlueColor = UIColor(red: 29/255, green: 161/255, blue: 242/255, alpha: 1)
}
