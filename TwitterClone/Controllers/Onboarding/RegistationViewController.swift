//
//  RegistationViewController.swift
//  TwitterClone
//
//  Created by Minh Hoàng Nguyễn Viết  on 07/05/2023.
//

import UIKit
import Combine

class RegistationViewController: UIViewController {
    private var viewModel = AuthenticationViewModel()
    private var subscription: Set<AnyCancellable> = []

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        view.addSubview(registerTitleLabel)
        view.addSubview(emailTextField)
        view.addSubview(passwordTextField)
        view.addSubview(registerButton)
        view.addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(dismissKeyboard)))
        registerButton.addTarget(self, action: #selector(didTapRegister), for: .touchUpInside)
        configureConstraints()
        bindView()
    }

    @objc func didTapRegister()  {
        Task{
            await executeCreatingUser()
        }
    }

    private func executeCreatingUser() async{
        await viewModel.createUser()
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    private func bindView() {
        emailTextField.addTarget(self, action: #selector(didChangeEmail), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(didChangePassword), for: .editingChanged)

        viewModel.$isValidAuthenticationForm.sink { [weak self] isValidForm in
            self?.registerButton.isEnabled = isValidForm
        }.store(in: &subscription)

        viewModel.$user.sink { [weak self] user in
            guard user != nil else { return }
            DispatchQueue.main.async {
                guard let onboardingVC = self?.navigationController?.viewControllers.first as? OnboardingViewController else {return}
                onboardingVC.dismiss(animated: true)
            }

        }.store(in: &subscription)

        viewModel.$error.sink { [weak self] errorMessage in
            guard let errorMessage = errorMessage else {return}
            DispatchQueue.main.async {
                self?.presentAlert(with: errorMessage)
            }

        }.store(in: &subscription)
    }

    private func presentAlert(with error: String){
        let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        let okayButton = UIAlertAction(title: "OK", style: .default)
        alert.addAction(okayButton)
        present(alert, animated: true)
    }

    @objc private func didChangeEmail(){
        viewModel.email = emailTextField.text
        viewModel.validateRegistrationForm()
    }

    @objc private func didChangePassword(){
        viewModel.password = passwordTextField.text
        viewModel.validateRegistrationForm()
    }

    private func configureConstraints() {
        let registerTitleLabelConstraints = [
            registerTitleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            registerTitleLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20)
        ]

        let emailTextFieldConstraints = [
            emailTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            emailTextField.topAnchor.constraint(equalTo: registerTitleLabel.bottomAnchor, constant:  20),
            emailTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:  -20),
            emailTextField.heightAnchor.constraint(equalToConstant: 60)
        ]


        let passwordTextFieldConstraints = [
            passwordTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            passwordTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant:  15),
            passwordTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant:  -20),
            passwordTextField.heightAnchor.constraint(equalToConstant: 60)
        ]

        let registerButtonConstraints = [
            registerButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            registerButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant:  20),
            registerButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 20),
            registerButton.heightAnchor.constraint(equalToConstant: 50)
        ]


        NSLayoutConstraint.activate(registerTitleLabelConstraints)
        NSLayoutConstraint.activate(emailTextFieldConstraints)
        NSLayoutConstraint.activate(passwordTextFieldConstraints)
        NSLayoutConstraint.activate(registerButtonConstraints)
    }

    private let registerTitleLabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Create your account"
        label.font = .systemFont(ofSize: 32, weight: .bold)

        return label
    }()

    private let emailTextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [
            NSAttributedString.Key.foregroundColor: UIColor.gray
        ])
        textField.keyboardType = .emailAddress
        textField.returnKeyType = .next

        return textField
    }()

    private let passwordTextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [
            NSAttributedString.Key.foregroundColor: UIColor.gray
        ])
        textField.isSecureTextEntry = true

        return textField
    }()

    private let registerButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Create account", for: .normal)
        button.tintColor = .white
        button.titleLabel?.font = .systemFont(ofSize: 16, weight: .bold)
        button.backgroundColor = .twitterBlueColor
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 25
        button.isEnabled = false

        return button
    }()
}
