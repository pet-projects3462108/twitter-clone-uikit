//
//  ViewController.swift
//  TwitterClone
//
//  Created by Minh Hoàng Nguyễn Viết  on 17/04/2023.
//

import UIKit

class MainTabbarViewController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .systemBackground
        let homeTab = UINavigationController(rootViewController: HomeViewController())
        let searchTab = UINavigationController(rootViewController: SearchViewController())
        let notificationTab = UINavigationController(rootViewController: NotificationViewController())
        let directMessagesTab = UINavigationController(rootViewController: DirectMessagesViewController())

        homeTab.tabBarItem.image = UIImage(systemName: "house")
        homeTab.tabBarItem.selectedImage = UIImage(systemName: "house.fill")

        searchTab.tabBarItem.image = UIImage(systemName: "magnifyingglass")

        notificationTab.tabBarItem.image = UIImage(systemName: "bell")
        notificationTab.tabBarItem.selectedImage = UIImage(systemName: "bell.fill")

        directMessagesTab.tabBarItem.image = UIImage(systemName: "envelope")
        directMessagesTab.tabBarItem.selectedImage = UIImage(systemName: "envelope.fill")

        setViewControllers([homeTab, searchTab, notificationTab, directMessagesTab], animated: true)
    }
}
