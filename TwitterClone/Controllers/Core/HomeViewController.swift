//
//  HomeViewController.swift
//  TwitterClone
//
//  Created by Minh Hoàng Nguyễn Viết  on 17/04/2023.
//

import FirebaseAuth
import UIKit
import Combine

class HomeViewController: UIViewController {
    private var viewModel = HomeViewModel()
    private var subscription: Set<AnyCancellable> = []

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(timelineTableView)
        view.addSubview(composeTweetButton)
        timelineTableView.delegate = self
        timelineTableView.dataSource = self
        configureNavigationBar()
        bindView()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        timelineTableView.frame = view.frame
        configureConstraints()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
        handleAuthentication()
        Task {
            await viewModel.retrieveUser()
        }
    }

    private func handleAuthentication() {
        if Auth.auth().currentUser == nil {
            let onboardingFlow = UINavigationController(rootViewController: OnboardingViewController())
            onboardingFlow.modalPresentationStyle = .fullScreen
            present(onboardingFlow, animated: false)
        }
    }

    private func bindView() {
        viewModel.$user.sink { [weak self] user in
            guard let user = user else { return }
            if !user.isUserOnBoarded {
                self?.completeUserOnboarding()
            }
        }.store(in: &subscription)

        viewModel.$tweets.sink { [weak self] _ in
            DispatchQueue.main.async {
                self?.timelineTableView.reloadData()
            }
        }.store(in: &subscription)
    }

    private func completeUserOnboarding() {
        DispatchQueue.main.async { [weak self] in
            let profileFormVC = ProfileFormViewController()
            self?.present(profileFormVC, animated: true)
        }
    }

    private func configureConstraints() {
        let composeTweetButtonConstraints = [
            composeTweetButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -25),
            composeTweetButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -120),
            composeTweetButton.widthAnchor.constraint(equalToConstant: 60),
            composeTweetButton.heightAnchor.constraint(equalToConstant: 60)

        ]

        NSLayoutConstraint.activate(composeTweetButtonConstraints)
    }

    private func configureNavigationBar() {
        let logoSize = 36
        let logoImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: logoSize, height: logoSize))
        logoImageView.contentMode = .scaleAspectFill
        logoImageView.image = UIImage(named: "TwitterLogo")

        let middleView = UIView(frame: CGRect(x: 0, y: 0, width: logoSize, height: logoSize))
        middleView.addSubview(logoImageView)
        navigationItem.titleView = middleView

        let profileImage = UIImage(systemName: "person")
        let logoutImage = UIImage(systemName: "rectangle.portrait.and.arrow.right")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: profileImage, style: .plain, target: self, action: #selector(didTapProfile))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: logoutImage, style: .plain, target: self, action: #selector(didTapLogout))
    }

    @objc private func didTapProfile() {
        let profileViewController = ProfileViewController()
        navigationController?.pushViewController(profileViewController, animated: true)
    }

    @objc private func didTapLogout() {
        try? Auth.auth().signOut()
        handleAuthentication()
    }

    private let timelineTableView: UITableView = {
        let tableView = UITableView()
        tableView.register(TweetTableViewCell.self, forCellReuseIdentifier: TweetTableViewCell.identifier)

        return tableView
    }()

    private lazy var composeTweetButton: UIButton = {
        let button = UIButton(type: .system, primaryAction: UIAction { [weak self] _ in
            let tweetComposeVC = UINavigationController(rootViewController: TweetComposeViewController())
            tweetComposeVC.modalPresentationStyle = .fullScreen
            self?.present(tweetComposeVC, animated: true)

        })
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .twitterBlueColor
        let plusSign = UIImage(systemName: "plus", withConfiguration: UIImage.SymbolConfiguration(pointSize: 18, weight: .bold))?.withTintColor(.white, renderingMode: .alwaysOriginal)
        button.setImage(plusSign, for: .normal)
        button.layer.cornerRadius = 30
        button.clipsToBounds = true

        return button
    }()
}

// MARK: - Table View

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.tweets.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TweetTableViewCell.identifier, for: indexPath) as? TweetTableViewCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        cell.configureTweet(with: viewModel.tweets[indexPath.row])
        return cell
    }
}

// MARK: - Table delegate

extension HomeViewController: TweetTableViewCellDelegate {
    func tweetTableViewCellDidTapReply() {
        print("reply")
    }

    func tweetTableViewCellDidTapRetweet() {
        print("retweet")
    }

    func tweetTableViewCellDidTapLike() {
        print("like")
    }

    func tweetTableViewCellDidTapShare() {
        print("share")
    }
}
