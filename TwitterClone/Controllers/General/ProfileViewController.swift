//
//  ProfileViewController.swift
//  TwitterClone
//
//  Created by Minh Hoàng Nguyễn Viết  on 22/04/2023.
//

import UIKit
import Combine
import SDWebImage

class ProfileViewController: UIViewController {
    private var viewModel = ProfileViewModel()
    private var subscriptions: Set<AnyCancellable> = []
    private var isStatusBarHidden = true

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        navigationItem.title = "Profile"
        view.addSubview(profileTableView)
        view.addSubview(statusBar)
        profileTableView.delegate = self
        profileTableView.dataSource = self
        profileTableView.tableHeaderView = headerView
        profileTableView.contentInsetAdjustmentBehavior = .never
        navigationController?.navigationBar.isHidden = true
        configureContraints()
        bindView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Task{
            await viewModel.retreiveUser()
        }
    }
    private func bindView() {
        viewModel.$user.sink { [weak self] user in
            if let user = user {
                DispatchQueue.main.async {
                    self?.headerView.displayNameLabel.text = user.displayName
                    self?.headerView.usernameLabel.text = "@\(user.userName)"
                    self?.headerView.followerCountLabel.text = "\(user.followerCount)"
                    self?.headerView.followingCountLabel.text = "\(user.followingCount)"
                    self?.headerView.joinDateLabel.text = self?.viewModel.getFormatedDate(with: user.createdOn)
                    self?.headerView.userBioLabel.text = "\(user.bio)"
                    self?.headerView.profileAvatarImageView.sd_setImage(with: URL(string: user.avatarPath))
                }
            }
        }.store(in: &subscriptions)
    }

    private func configureContraints() {
        let profileTableViewConstraints = [
            profileTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            profileTableView.topAnchor.constraint(equalTo: view.topAnchor),
            profileTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            profileTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ]

        let statusBarHeight = UIApplication.shared.connectedScenes
            .filter {$0.activationState == .foregroundActive }
            .map {$0 as? UIWindowScene }
            .compactMap { $0 }
            .first?.windows
            .filter({ $0.isKeyWindow }).first?
            .windowScene?.statusBarManager?.statusBarFrame.height ?? 0

        let statusBarConstraints = [
            statusBar.topAnchor.constraint(equalTo: view.topAnchor),
            statusBar.widthAnchor.constraint(equalTo: view.widthAnchor),
            statusBar.heightAnchor.constraint(equalToConstant: statusBarHeight)
        ]

        NSLayoutConstraint.activate(profileTableViewConstraints)
        NSLayoutConstraint.activate(statusBarConstraints)
    }

    private lazy var headerView =  ProfileTableViewHeader(frame: CGRect(x: 0, y: 0, width: profileTableView.frame.width, height: 380))


    private let statusBar: UIView = {
        let view = UIView()
        view.backgroundColor = .systemBackground
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.opacity = 0

        return view
    }()

    private let profileTableView: UITableView = {
        let tableView = UITableView()
        tableView.register(TweetTableViewCell.self, forCellReuseIdentifier: TweetTableViewCell.identifier)
        tableView.translatesAutoresizingMaskIntoConstraints = false

        return tableView
    }()
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TweetTableViewCell.identifier, for: indexPath) as? TweetTableViewCell else {
            return UITableViewCell()
        }

        return cell
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let dy = scrollView.contentOffset.y

        if dy > 150, isStatusBarHidden {
            isStatusBarHidden = false
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) { [weak self] in
                self?.statusBar.layer.opacity = 1
            }
        } else if dy < 0, !isStatusBarHidden {
            isStatusBarHidden = true
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
                self.statusBar.layer.opacity = 0
            }
        }
    }
}
