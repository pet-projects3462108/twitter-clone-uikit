//
//  TweetComposeViewController.swift
//  TwitterClone
//
//  Created by Minh Hoàng Nguyễn Viết  on 17/05/2023.
//

import UIKit
import Combine

class TweetComposeViewController: UIViewController {
    private var viewModel = TweetComposeViewModel()
    private var subscriptions: Set<AnyCancellable> = []

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Task{
            await viewModel.getUserData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        title = "Tweet"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(didTapCancel))
        tweetContentTextView.delegate = self 
        view.addSubview(tweetButton)
        view.addSubview(tweetContentTextView)

        configureConstraint()
        bindView()
    }

    @objc private func didTapCancel() {
        dismiss(animated: true)
    }

    private func bindView() {
        viewModel.$isValidTweet.sink { [weak self] state in
            self?.tweetButton.isEnabled = state
        }.store(in: &subscriptions)

        viewModel.$shouldDismissComposer.sink { [weak self] state in
            if state {
                DispatchQueue.main.async {
                    self?.dismiss(animated: true)
                }
            }
        }.store(in: &subscriptions)
    }

    private func configureConstraint () {
        let tweetButtonConstraints = [
            tweetButton.bottomAnchor.constraint(equalTo: view.keyboardLayoutGuide.topAnchor, constant: -10),
            tweetButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            tweetButton.widthAnchor.constraint(equalToConstant: 120),
            tweetButton.heightAnchor.constraint(equalToConstant: 40)
        ]

        let tweetContentTextViewConstraints = [
            tweetContentTextView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tweetContentTextView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 15),
            tweetContentTextView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -15),
            tweetContentTextView.bottomAnchor.constraint(equalTo: tweetButton.topAnchor, constant: -10)
        ]

        NSLayoutConstraint.activate(tweetButtonConstraints)
        NSLayoutConstraint.activate(tweetContentTextViewConstraints)
    }

    private lazy var tweetButton: UIButton = {
        let button = UIButton(type: .system, primaryAction: UIAction { [weak self] _ in
            Task{
               await self?.viewModel.createTweet()
            }
        })
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .twitterBlueColor
        button.setTitle("Tweet", for: .normal)
        button.layer.cornerRadius = 20
        button.clipsToBounds = true
        button.setTitleColor(.white, for: .normal)
        button.setTitleColor(.white.withAlphaComponent(0.7), for: .disabled)
        button.titleLabel?.font = .systemFont(ofSize: 16, weight: .semibold)
        button.isEnabled = false

        return button
    }()

    private let tweetContentTextView: UITextView = {
        let textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.layer.masksToBounds = true
        textView.layer.cornerRadius = 8
        textView.textContainerInset = .init(top: 15, left: 15, bottom: 15, right: 15)
        textView.text = "What's happening"
        textView.textColor = .gray
        textView.font = .systemFont(ofSize: 16)

        return textView
    }()


}

extension TweetComposeViewController: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == .gray{
            textView.textColor = .label
            textView.text = ""
        }

    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "What's happening"
            textView.textColor = .gray
        }
    }

    func textViewDidChange(_ textView: UITextView) {
        viewModel.tweetContent = textView.text
        viewModel.validateTweet()
    }
}
