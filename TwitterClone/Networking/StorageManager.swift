//
//  StorageManager.swift
//  TwitterClone
//
//  Created by Minh Hoàng Nguyễn Viết  on 14/05/2023.
//

import Foundation
import FirebaseStorage

enum FireStorageError : Error {
    case invalidImageId

}

class StorageManager{
    static let shared = StorageManager()

    let storage = Storage.storage()

    func downloadUrl(for id: String?) async throws -> URL {
        guard let id = id else { throw FireStorageError.invalidImageId  }
        return try await storage.reference(withPath: id).downloadURL()
    }

    func uploadProfilePhoto(with randomId: String, image: Data, metaData: StorageMetadata) async throws -> StorageMetadata {
        return try await storage.reference().child("images/\(randomId).jpg").putDataAsync(image, metadata: metaData)
    }
}

