//
//  DatabaseManager.swift
//  TwitterClone
//
//  Created by Minh Hoàng Nguyễn Viết  on 14/05/2023.
//

import FirebaseAuth
import FirebaseFirestore
import FirebaseFirestoreSwift
import Foundation

class DatabaseManager {
    static let shared = DatabaseManager()

    private let database = Firestore.firestore()
    private let userPath = "users"
    private let tweetPath = "tweets"

    func collectionUsers(add user: User) async throws {
        let twitterUser = TwitterUser(from: user)
        try await database.collection(userPath).document(twitterUser.id).setData(twitterUser.dictionary ?? [:])
    }

    func collectionUsers(retrieve id: String) async throws -> TwitterUser {
        return try await database.collection(userPath).document(id).getDocument().data(as: TwitterUser.self)
    }

    func collectionUsers(updateFields: [String: Any], for id: String) async throws {
        try await database.collection(userPath).document(id).updateData(updateFields)
    }

    func collectionTweets(add tweet: Tweet) async throws {
        try await database.collection(tweetPath).document(tweet.id).setData(tweet.dictionary ?? [:])
    }

    func collectionTweets(retrieveTweets forUserID: String) async throws -> [Tweet] {
        try await database.collection(tweetPath).whereField("authorID", isEqualTo: forUserID).getDocuments().documents.map({ snapShot in
            try snapShot.data(as: Tweet.self)
        })
    }
}
