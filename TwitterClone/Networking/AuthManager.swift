//
//  AuthManager.swift
//  TwitterClone
//
//  Created by Minh Hoàng Nguyễn Viết  on 07/05/2023.
//

import Foundation
import FirebaseAuth
import Combine

class AuthManager{
    static let shared = AuthManager()

    private init() {}

    func registerUser(email: String, password: String) async throws -> AuthDataResult{
        try await Auth.auth().createUser(withEmail: email, password: password)
    }

    func loginUser(email: String, password: String) async throws -> AuthDataResult{
        try await Auth.auth().signIn(withEmail: email, password: password)
    }
}
